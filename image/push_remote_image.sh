source ../settings.sh

echo "Push remote images $DOCKER_REGISTRY/$IMAGENAME"
echo

docker image push --all-tags $DOCKER_REGISTRY/$IMAGENAME
