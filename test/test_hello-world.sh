source ../settings.sh

echo "Showing hello world statistics."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	--volume "${PWD}:/usr/src/myscripts" \
	$IMAGENAME \
		hello-world.R
